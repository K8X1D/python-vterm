R-vterm.el:
	@wget https://raw.githubusercontent.com/shg/julia-vterm.el/master/julia-vterm.el 
	mv julia-vterm.el python-vterm.el
	sed -i 's/extends julia-mode/extends python-mode/g' python-vterm.el
	sed -i 's/julia/python/g' python-vterm.el
	sed -i 's/Julia/python/g' python-vterm.el
	sed -i 's/include(\"%s\")/import(\"%s\")/g' python-vterm.el

clean:
	rm *.el
