# -*- eval: (visual-line-mode 1) -*-
#+STARTUP: showall

* python-vterm

python-vterm provides a major-mode for an inferior R process (or REPL) that runs in vterm, and a minor-mode that extends ess-r-mode with the ability to interact with the inferior python process.

It is a clone of the great [[https://github.com/shg/julia-vterm.el][julia-vterm]] with the necessay adjustment for python.


** Installation

** How to use

** Configuration
It's possible to adjust the R program used through to following variable.
#+begin_src emacs-lisp
  (setq python-vterm-repl-program "/path/to/program")
#+end_src

Hence, it's possible to use something like [[https://bpython-interpreter.org/][bpython]] instead of python native console by including the following in your config.
#+begin_src emacs-lisp
  (setq python-vterm-repl-program "bpython")
#+end_src

** Key bindings

*** python-vterm-mode

#+begin_example
Key         Command / Description
------------------------------------------------------------------------------------------
C-c C-z     python-vterm-switch-to-repl-buffer
            Switch to the paired REPL buffer or to the one with a specified session name.
            With prefix ARG, prompt for session name.

C-<return>  python-vterm-send-region-or-current-line
            Send the content of the region if the region is active, or send the current
            line.

C-c C-b     python-vterm-send-buffer
            Send the whole content of the script buffer to the R REPL line by line.

C-c C-i     python-vterm-send-include-buffer-file
            Send a line to evaluate the buffer's file using include() to the R REPL.
            With prefix ARG, Revise.includet() is used instead.

C-c C-d     python-vterm-send-cd-to-buffer-directory
            Send cd() function call to the R REPL to change the current working
            directory of REPL to the buffer's directory.
#+end_example

*** python-vterm-repl-mode

#+begin_example
Key         Command / Description
------------------------------------------------------------------------------------------
C-c C-z     python-vterm-repl-switch-to-script-buffer
            Switch to the script buffer that is paired with the current R REPL buffer.

M-k         python-vterm-repl-clear-buffer
            Clear the content of the R REPL buffer.

C-c C-t     python-vterm-repl-copy-mode
            Enter copy mode.
#+end_example

*** python-vterm-repl-mode (copy mode)

#+begin_example
Key         Command / Description
------------------------------------------------------------------------------------------
C-c C-t     python-vterm-repl-copy-mode
            Exit copy mode.

<return>    python-vterm-repl-copy-mode-done
            Copy the region to the kill ring and exit copy mode.

C-c C-r     vterm-reset-cursor-point
            Call the vterm command that moves point to where it should be.
#+end_example
